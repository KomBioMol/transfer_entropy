# Transfer entropy

A versatile and fast Python tool for the calculation of transfer entropy matrices from both continuous and discrete data series, written by Miłosz Wieczór.

If you use this script, please cite the following work:
M. Wieczór, J. Czub. How proteins bind to DNA: target discrimination and dynamic sequence search by the telomeric protein TRF1. Nucleic Acids Res. 2017, 45, 7643–7654.

## Getting Started

The script is ready to use on any machine employed with a working Python distribution. Run "python transfer\_entropy.py -h" to see available options.

To learn more about the topic, please refer to a paper by Kamberaj and van der Vaart (Biophys J. 2009 Sep 16; 97(6): 1747–1755)

### Prerequisites

Two modules - numpy and multiprocessing - are required for full functionality; if not available, both can be installed using pip:

```
pip install numpy multiprocessing
```

### Options

#### Simple usage

  The script can read multiple data files at once, with columns corresponding to individual observables in the time series (e.g. from an MD simulation). It is assumed that the observables are continuous, so that discretization is performed first, as transfer entropy is calculated for discrete variables.

  The file containing a list of paths to data files has to be supplied with the option -f. A separate file (passed with the -d option) is needed for discretization; if the file only contains one line of integers, it is assumed that these correspond to the number of bins/discrete states each column of observables will be divided into, with equal spacing. (If your data is already discretized, this will work too.) For example, if the first column has to be discretized into 5 states, second into 2 and third into 4, the file should read:
```
5 2 4
```

  Alternatively, if each column has to have custom thresholds, these can be specified in columns in the threshold file. In this case, columns of thresholds should correspond to columns of observables, and the placeholder 'x' should be inserted to fill in the missing space. A sample file (in analogy to the above example) would read:
```
0.3  0.0  1
0.35  x   2
0.4   x   5
0.6   x   x
```

Note that in this case, thresholds need to be sorted. Specifying N thresholds will produce N+1 discrete states.

#### Parallel run

  To run on multiple CPUs, pass the -n num\_of\_processes option. Since individual matrix elements can be computed separately, this should provide an almost linear speedup.

#### Data shuffling

  The -s option enables shuffling of data in order to estimate the contribution from statistical noise. After the actual calculation, it is useful to repeat it on shuffled data a couple of times and then subtract the mean shuffled matrix from the ordered one.

### Examples


  General usage (parameters in square brackets are optional):

```
python transfer_entropy.py -f list_of_datafile_names -d thresholds_file [-l lagtime -n n_proc -o output_file -s]
```
