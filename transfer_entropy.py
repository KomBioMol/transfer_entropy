import numpy as np
import argparse
import multiprocess # might need to be installed: sudo pip install multiprocess

# input is K NxM matrices, N timeframes for M observables (columns) in K trajectories
# we assume history length is = 1, i.e. m = 0 in the original formulation
# complexity should be O(N*M^2)


def main():
    args = parse_args()
    files = [str(f).strip() for f in open(args.filenames)]
    (data, nstates) = read_all_data(files, args.thresholds,args.shuf)
    lagtime = args.lagtime
    transfer_entr = np.zeros((data[0].shape[1],data[0].shape[1]))
    h_vals = np.zeros(data[0].shape[1])
    pairs = []
    for i in xrange(data[0].shape[1]):
        for j in xrange(data[0].shape[1]):
            if i!=j:
                pairs.append((i,j)) # stack the inputs
    proc_pool = multiprocess.Pool(processes=args.nproc)
    while pairs: # actual calculations
        temp_pairs = pairs[-args.nproc:] # get the inputs off the stack
        print temp_pairs
        entries = proc_pool.map(lambda (q, w): get_transfer_entropy([dat[:,q] for dat in data],[dat[:,w] for dat in data],nstates[q],nstates[w],lagtime, h_vals[q]), temp_pairs)
        #entries = map(lambda (q, w): get_transfer_entropy([dana[:,q] for dana in dane],[dana[:,w] for dana in dane],nstates[q],nstates[w],lagtime, h_vals[q]), temp_pairs) # non-parallel version used for debugging
        for proc in range(len(temp_pairs)):
            transfer_entr[temp_pairs[proc]] = entries[proc][0] # fill in the T_ij matrix
            h_vals[temp_pairs[proc][0]] = entries[proc][1] # fill in the h_i vector
        del pairs[-args.nproc:]
    # when everything is done, normalize the results + save to file
    normed_te = transfer_entr * 0.0 # D_ij in the original formulation
    for i in xrange(data[0].shape[1]): # no need to optimize this, operates on small matrices
        for j in xrange(data[0].shape[1]):
            if i!=j:
                normed_te[i,j] = transfer_entr[i,j]/h_vals[i] - transfer_entr[j,i]/h_vals[j]
    np.savetxt(args.outfile, normed_te,fmt='%9.5f')

def parse_args():
    parser = argparse.ArgumentParser(description='Calculates the transfer entropy matrix T_ij from a set of simulation-derived observables.')
    parser.add_argument('-f', type=str, dest='filenames', help='file containing paths to data files')
    parser.add_argument('-o', type=str, dest='outfile', default="tentr.dat", help='output file, default tentr.dat')
    parser.add_argument('-l', type=int, dest='lagtime', default=1, help='lagtime (in data timestep units), default 1')
    parser.add_argument('-n', type=int, dest='nproc', default=1, help='number of processes to use for parallelization, default 1')
    parser.add_argument('-s', dest='shuf', action='store_true', help='shuffle data to estimate numerical noise, use as flag')
    parser.add_argument('-d', type=str, dest='thresholds', help='file containing [a] discretization thresholds (matrix-like, in rows for each data column, with "x" marking empty entries) or [b] (single-line) numbers of discrete states per column to produce w/ equal spacing')
    args = parser.parse_args()
    return args

def get_transition_counts(cols_i, cols_j, nstates_i, nstates_j, lagtime=1):
    """computes the 3D transition/history matrix for m = 0"""
    tcount_matrix = np.zeros((nstates_i, nstates_i, nstates_j), dtype=np.int32)
    for (col_i, col_j) in zip(cols_i, cols_j):
        assert len(col_i) == len(col_j)
        assert lagtime < len(col_i)
        cikk = np.matrix(col_i[lagtime:],dtype=np.int16).T
        cik = np.matrix(col_i[:-lagtime],dtype=np.int16).T
        cjk = np.matrix(col_j[:-lagtime],dtype=np.int16).T
        merge = np.array(np.hstack((cikk,cik,cjk)))
        tcount_matrix += np.histogramdd(merge,bins=(nstates_i,nstates_i,nstates_j),range=((0,nstates_i-1),(0,nstates_i-1),(0,nstates_j-1)))[0]
    return tcount_matrix

def get_p_joint(tcount_matrix):
    """computes the 3D matrix p(ik+1, ik, jk)"""
    norm = np.sum(tcount_matrix)
    return tcount_matrix/float(norm)

def get_p_condit(tcount_matrix):
    """computes the 3D matrix p(ik+1 | ik, jk)"""
    p_condit = tcount_matrix * 0.0
    for i in xrange(tcount_matrix.shape[1]):
        for j in xrange(tcount_matrix.shape[2]):
            q = np.sum(tcount_matrix[:, i, j])
            if q:
                p_condit[:, i, j] = tcount_matrix[:, i, j] / float(q)
    return p_condit

def get_p_single(tcount_matrix):
    """computes the 2D matrices: p(ik+1 | ik) and p(ik+1, ik)"""
    m2d = np.sum(tcount_matrix,2)
    p_single = m2d * 0.0
    for i in xrange(m2d.shape[1]):
        q = np.sum(m2d[:,i])
        if q:
            p_single[:,i] = m2d[:,i] / float(q)
    norm = np.sum(m2d)
    return p_single, m2d/float(norm)

def get_transfer_entropy(cols_i, cols_j, nstates_i, nstates_j, lagtime=1, hval=0.0):
    """reads two lists of columns (coords i, j from the set of K trajectories),
     calculates the count and probability matrices
     and returns T_ij (element of t.entr. matrix) and h_i"""
    tcount_matrix = get_transition_counts(cols_i,cols_j,nstates_i,nstates_j,lagtime)
    p_joint = get_p_joint(tcount_matrix) # 3D matrix p(ik+1, ik, jk)
    p_condit = get_p_condit(tcount_matrix) # 3D matrix p(ik+1 | ik, jk)
    p_single, p_sjoint = get_p_single(tcount_matrix) # 2D matrix p(ik+1 | ik)
    Tij = 0.0
    hi = 0.0
    for (col_i, col_j) in zip(cols_i, cols_j):
        cikk = np.matrix(col_i[lagtime:],dtype=np.int16).T
        cik = np.matrix(col_i[:-lagtime],dtype=np.int16).T
        cjk = np.matrix(col_j[:-lagtime],dtype=np.int16).T
        prefact = p_joint[cikk, cik, cjk]
        num = p_condit[cikk,cik,cjk]
        den = p_single[cikk,cik]
        Tij += np.sum(prefact * np.log(num/den)) # is now 10-20x faster thanks to vectorization
        if not hval:
            hi -= np.sum(p_sjoint[cikk,cik] * np.log(den))
    if hval > 0:
        hi = hval
    return Tij, hi

def convert_cont_to_intstates(matr, thresholds):
    """discretizes a continuous coordinate into a set of states 0, 1, ..., N,
    where N is the number of thresholds for the given column"""
    discr_states = np.zeros(matr.shape,np.int16)
    for i in range(1,thresholds.shape[0]):
        discr_states += np.logical_and(matr>thresholds[i-1,:], matr<=thresholds[i,:])*i
    discr_states += (matr>thresholds[-1,:])*thresholds.shape[0]
    return discr_states

def read_all_data(file_list, thresholds_file, shuffle=False):
    """reads data based on a list of data files + file with threshold settings for discretizing;
    returns a list of numpy arrays corresponding to any number of trajectories
    and the number of discrete states for each column"""
    test = len([linia for linia in open(thresholds_file)]) # a test whether we need min/max values in order not to spend too much time on that step
    if test == 1:
        data_list = []
        temp0 = np.loadtxt(file_list[0])
        minims = np.min(temp0,0)
        maxims = np.max(temp0, 0)
        data_list.append(temp0)
        for filename in file_list[1:]:
            temp = np.loadtxt(filename)
            minims = np.min(np.vstack((minims,temp)),0)
            maxims = np.max(np.vstack((maxims, temp)),0)
            if shuffle:
                data_list.append(np.random.permutation(temp))
            else:
                data_list.append(temp)
        (thresholds, nst) = parse_thresholds(thresholds_file, maxims, minims)
        final_list = []
        for data in data_list:
            final_list.append(convert_cont_to_intstates(data, thresholds))
    else:
        final_list = []
        (thresholds, nst) = parse_thresholds(thresholds_file)
        for filename in file_list:
            data = np.loadtxt(filename)
            if shuffle:
                final_list.append(convert_cont_to_intstates(np.random.permutation(data), thresholds))
            else:
                final_list.append(convert_cont_to_intstates(data, thresholds))
    return final_list, nst

def parse_thresholds(thresholds_file, maxvals=None, minvals=None):
    """For each data column we need a set of thresholds that discretize the continuous variable;
    this requires a threshold matrix parsed by this function. 
    Each column in the thresholds file corresponds to a column in the data file.
    If only one line exists, the values are number of discrete states produced for each column
    by dividing the min:max interval into equal spacing.
    If there is more than one line, they are treated as multiple thresholds for each column,
    with x marking empty cells (not every data column needs to have the same number of states).
    Sample input for 3 columns that will produce 5, 5 and 3 discrete states based on thresholds:
    0.10 0.15 -0.05
    0.25 0.30  0.05
    0.50 0.60  x
    1.00 1.50  x
    or, with equal spacing:
    5 5 3
    Note that threshold values need to be sorted!
    """
    thresh = np.genfromtxt(thresholds_file,missing_values='x', filling_values=np.infty)
    if thresh.shape[0] == thresh.size: # 1D case
        thresholds = np.ones((thresh.max()-1,thresh.shape[0]))*np.infty
        for col in range(thresholds.shape[1]):
            thresholds[:thresh[col]-1,col] = np.linspace(minvals[col],maxvals[col],thresh[col]+1)[1:-1]
    nstates = np.sum(thresholds != np.infty,0) + 1
    return thresholds, nstates

main()
